import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuardService } from 'src/services/auth-guard.service';
import { EditEmployeeComponent } from './componentes/edit-employee/edit-employee.component';
import { EmpleadosComponent } from './componentes/empleados/empleados.component';
import { GruposComponent } from './componentes/grupos/grupos.component';
import { PrincipalComponent } from './componentes/principal/principal.component'


const routes: Routes = [
  {path: '', component: PrincipalComponent},
  // {path: 'empleados', component: EmpleadosComponent, canActivate: [AuthGuardService]},
  // {path: 'grupos', component: GruposComponent, canActivate: [AuthGuardService]},
  {path: 'empleados', component: EmpleadosComponent},
  {path: 'grupos', component: GruposComponent},
  {path: 'agregar/:id', component: EditEmployeeComponent, canActivate: [AuthGuardService]},
  {path: '**', component: PrincipalComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
