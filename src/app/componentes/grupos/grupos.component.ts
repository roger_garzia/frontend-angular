import { Component, OnInit } from '@angular/core';
import { EmpleadosService } from 'src/services/empleados.service';
import {CdkDragDrop, moveItemInArray, transferArrayItem} from '@angular/cdk/drag-drop';

@Component({
  selector: 'app-grupos',
  templateUrl: './grupos.component.html',
  styleUrls: ['./grupos.component.css']
})
export class GruposComponent implements OnInit {

  public groups: any = [];
  public users: any = [];
  searchFilter: string;

  constructor(private _empleadoService: EmpleadosService) { }

  ngOnInit() {
    this.getGroups();
  }

  getGroups() {
    this._empleadoService.getDataGroups().subscribe((data:any) => {
      this.groups = data.data.groups;
    });
  }

  getInfoDepto(id) {
    this._empleadoService.getUsersDepto(id).subscribe((data: any) => {
      this.users = data.data.employees;
    });
  } 

  onDrop(event: CdkDragDrop<string[]>) {
    if (event.previousContainer !== event.container) {
      transferArrayItem(event.previousContainer.data, event.container.data,
        event.previousIndex, event.currentIndex)
    } else {
      moveItemInArray(this.users, event.previousIndex, event.currentIndex);
    }
  }

}
