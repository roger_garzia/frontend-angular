import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Employee } from 'src/models/modelEmployees';
import { EmpleadosService } from '../../../services/empleados.service';

@Component({
  selector: 'app-empleados',
  templateUrl: './empleados.component.html',
  styleUrls: ['./empleados.component.css']
})
export class EmpleadosComponent implements OnInit {

  employees;
  registerForm: FormGroup;
  newEmployee: Employee;

  public page: number;
  filterpost: string;

  constructor(private empleadosService: EmpleadosService, private fb: FormBuilder) {
    this.registerForm = this.fb.group({
      name: ['', [Validators.maxLength(30), Validators.required]],
      last_name: ['', [Validators.maxLength(30), Validators.required]],
      birthday: ['', Validators.required]
    });
  }

  reset() {
    this.registerForm.patchValue({
      name: '',
      last_name: '',
      birthday: ''
    });
  }
  
  ngOnInit() {
    this.getEmployees();  
  }

  getEmployees() {
    this.empleadosService.getDataEmployees().subscribe((data: any) => {
      this.employees = data.data.employees;
    })
  }

  validate() {
    if(!this.registerForm.valid) {
      alert('No se esta cumpliendo alguna regla de Validación');
      return;
    } else {
      this.agregarEmpleado();
    }
  }

  agregarEmpleado() {
    this.newEmployee = this.registerForm.value;
    this.empleadosService.addEmployee(this.newEmployee).subscribe(employee => this.employees.push(employee));
    this.reset();
  }

}
