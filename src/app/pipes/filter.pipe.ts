import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform {

  transform(value: any, arg: any): any {
    const result = [];
    if(!arg || arg.length < 3 ) return value;
    for(const employees of value) {
      if(employees.name.indexOf(arg) > -1) {
        result.push(employees);
      };
    };
    return result;
  }

}
