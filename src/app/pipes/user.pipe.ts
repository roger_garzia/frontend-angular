import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'user'
})
export class UserPipe implements PipeTransform {

  transform(value: any, arg: any): any {
    const result = [];
    if(!arg || arg.length < 3) return value;
    for(const groups of value) {
      if(groups.name.indexOf(arg) > -1) {
        result.push(groups);
      };
    };
    return result;
  }

}
