import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common//http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './componentes/header/header.component';
import { FooterComponent } from './componentes/footer/footer.component';
import { EmpleadosComponent } from './componentes/empleados/empleados.component';
import { GruposComponent } from './componentes/grupos/grupos.component';
import { PrincipalComponent } from './componentes/principal/principal.component';
import { EditEmployeeComponent } from './componentes/edit-employee/edit-employee.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

// Servicios
import { EmpleadosService } from 'src/services/empleados.service';
import { AuthGuardService } from '../services/auth-guard.service';

// Dependencias
import {NgxPaginationModule} from 'ngx-pagination';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { FilterPipe } from './pipes/filter.pipe';
import { UserPipe } from './pipes/user.pipe';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    EmpleadosComponent,
    GruposComponent,
    PrincipalComponent,
    EditEmployeeComponent,
    FilterPipe,
    UserPipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    NgxPaginationModule,
    FormsModule,
    ReactiveFormsModule,
    DragDropModule
  ],
  providers: [EmpleadosService, AuthGuardService],
  bootstrap: [AppComponent]
})
export class AppModule { }
