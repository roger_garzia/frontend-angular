import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { LoginService } from './login.service';

@Injectable({
  providedIn: 'root'
})

export class AuthGuardService implements CanActivate {

  constructor(private _authService: LoginService, private router: Router) { }

  canActivate() {

      if (this._authService.login('roger')) {
        console.log('Aún no te encuentras Logueado');
        this.router.navigate(['/']);
        return false;
      }
      return true;

/*       if ( localStorage.getItem('access_token')) {
        return true;
      } else {
        this.router.navigate(['/auth']);
        return false;
      } */

  }
    
}
