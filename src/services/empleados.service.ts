import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { Employee } from '../models/modelEmployees';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EmpleadosService {

  constructor(private http: HttpClient) { }

  getDataEmployees(): Observable<Employee[]> {
    const URL = 'https://6edeayi7ch.execute-api.us-east-1.amazonaws.com/v1/examen/employees/rogelio_garcia';
    return this.http.get<Employee[]>(URL);
  }

  addEmployee(employee: Employee): Observable<Employee> {
    const path = 'https://6edeayi7ch.execute-api.us-east-1.amazonaws.com/v1/examen/employees/rogelio_garcia';
    return this.http.post<Employee>(path, employee);
  }

  getDataGroups() {
    const path = 'https://6edeayi7ch.execute-api.us-east-1.amazonaws.com/v1/examen/groups/rogelio_garcia';
    return this.http.get(path);
  }

  getUsersDepto(id: number) {
    const path = `https://6edeayi7ch.execute-api.us-east-1.amazonaws.com/v1/examen/employees/rogelio_garcia/getByGroup?id=${id}`;
    return this.http.get(path);
  }
}
