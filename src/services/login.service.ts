import { Injectable } from '@angular/core';
import { HttpErrorResponse, HttpClient } from '@angular/common/http';
import { throwError, Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { catchError } from 'rxjs/operators';
import { Router } from '@angular/router';


@Injectable({
  providedIn: 'root',
})

export class LoginService {

  constructor(private router: Router, private http: HttpClient) { }
  apiURL = `${environment.apiUrl}/api/users/change/password/admin/`;

  logout() {
    localStorage.removeItem('access_token');
    localStorage.removeItem('refresh_token');
    this.router.navigate(['/auth/login']);
  }

  login(user): Observable<any> {
     const body = {
       username: user.email,
       password: user.password
     };
     const url = `${environment.apiUrl}/api/token/`;
     return this.http.post<any>(url, body).pipe(catchError(this.handleError));
  }

  checkCredentials() {
    if (localStorage.getItem('user') === null) {
      this.router.navigate(['usuario']);
    }
  }

  // tslint:disable-next-line:variable-name
  refreshToken(refresh_token: any) {
    const URL = `${environment.apiUrl}/api/token/refresh/`;
    return this.http.post(URL, { refresh: refresh_token} );
  }

  sendCodeRecover(email) {
    const URL = `${environment.apiUrl}/api/users/recover/password/admin/`;
    return this.http.post(URL, {email});
  }

  checkCode(admin) {
    const URL = `${environment.apiUrl}/api/users/change/password/admin/`;
    return this.http.post(URL, admin);
  }

  private handleError(error: HttpErrorResponse) {
    return throwError(error);
  }

  private handleSuccess(response: any): Promise<any> {
    return Promise.resolve(response);
  }
}
