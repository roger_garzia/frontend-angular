## Comandos del Proyecto
-- npm install (Instalacion de Dependencias)
-- npm start (Arranque del Proyecto)

## Diseño a Conveniencia
-- Proyecto maquetado con Bootstrap 4
-- Version en Angular 9
-- Hojas de estilo en CSS3
-- UI/UX Responsive Web Design

## Implementacion
-- Se implementaron los Guardianes(CanActivate) para proteccion en paginas de navegacion,
   son funcionales solo que se comentan estas lineas debido a que no se implementa interface
   de autenticacion, pero el JWT se implementa para poder guardarse en el local storage.
   Tambien se implementa LogginService para poder ser usado en caso de que se requiera una interface
   para el registro de usuarios.
   
Observaciones: Tuve un detalle en la pagina de grupos, pero se logro completar la mayoria de requisitos
               solicitados
   
   


